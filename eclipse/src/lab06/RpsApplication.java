//Angela Sposato 1934695
package lab06;
import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;



public class RpsApplication extends Application {
	private RpsGame game = new RpsGame();
	public void start(Stage stage) {
		//game needs to be private
		
		Group root = new Group(); 
		Button rock = new Button("rock");
		Button paper = new Button("paper");
		Button scissors = new Button("scissors");
		HBox buttons = new HBox();
		TextField message = new TextField("Welcome!");
		message.setPrefWidth(200);
		TextField wins = new TextField("wins: 0");
		TextField losses = new TextField("losses: 0");
		TextField ties = new TextField("ties: 0");
		HBox textFields = new HBox();
		VBox overall = new VBox();
		RpsChoice rockChoice = new RpsChoice("rock", message, wins, losses, ties, game);
		RpsChoice paperChoice = new RpsChoice("paper", message, wins, losses, ties, game);
		RpsChoice scissorsChoice = new RpsChoice ("scissors", message, wins, losses, ties, game);
		rock.setOnAction(rockChoice);
		paper.setOnAction(paperChoice);
		scissors.setOnAction(scissorsChoice);
		buttons.getChildren().addAll(rock, paper, scissors);
		textFields.getChildren().addAll(message, wins, losses, ties);
		overall.getChildren().addAll(buttons, textFields);
		root.getChildren().add(overall);
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }

}

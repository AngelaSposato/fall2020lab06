package lab06;
import java.util.*;
public class TestClass {
	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter rock, paper, scissors");
		String userChoice = sc.nextLine();
		RpsGame newGame = new RpsGame();
		String result = newGame.playRound(userChoice);
		System.out.println(result);
	}
}

//Angela Sposato 1934695
package lab06;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;

public class RpsChoice implements EventHandler<ActionEvent>{
	private String userChoice;
	private TextField message;
	private TextField wins;
	private TextField losses;
	private TextField ties;
	private RpsGame game;
	public RpsChoice(String userChoice, TextField message, TextField wins, TextField losses, TextField ties, RpsGame game) {
		this.userChoice = userChoice;
		this.message = message;
		this.wins = wins;
		this.losses = losses;
		this.ties = ties;
		this.game = game;
	}
	@Override 
	public void handle(ActionEvent e) {
		String result = game.playRound(userChoice);
		message.setText(result);
		String winCount = Integer.toString(game.getWins());
		String lossCount = Integer.toString(game.getLosses());
		String tieCount = Integer.toString(game.getTies());
		wins.setText("wins: " + winCount);
		losses.setText("losses: " + lossCount);
		ties.setText("ties: " + tieCount);
	}
}

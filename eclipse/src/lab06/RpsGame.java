//Angela Sposato 1934695
package lab06;

import java.util.Random;

public class RpsGame {
	//stats for players, not cpu
	int wins = 0;
	int losses = 0; 
	int ties = 0;
	Random randGen = new Random();
	public int getWins() {
		return wins;
	}
	public int getLosses() {
		return losses;
	}
	public int getTies() {
		return ties;
	}
	public String playRound(String userChoice) {
		int cpuResult = randGen.nextInt(3);
		String cpuChoice = "";
		switch (cpuResult) {
			case 0: 
				cpuChoice = "rock";
				break;
			case 1:
				cpuChoice = "paper";
				break;
			case 2: 
				cpuChoice = "scissors";
				break;
		}
		//If user picks rock
		switch (userChoice) {
		case "rock":
			switch (cpuChoice) {
			//rock rock
			case "rock":
				ties++;
				return "Computer plays rock and there was a tie";
			//rock paper
			case "paper":
				losses++;
				return "Computer plays paper and computer wins";
			//rock scissors
			case "scissors":
				wins++;
				return "Computer plays scissors and computer loses";
			}
			break;
		//If user picks paper
		case "paper":
			switch (cpuChoice) {
			//paper rock
			case "rock":
				wins++;
				return "Computer plays rock and computer loses";
			//paper paper 
			case "paper":
				ties++;
				return "Computer plays paper and there was a tie";
			//paper scissors
			case "scissors":
				losses++;
				return "Computer plays scissors and computer wins";
			}
			break;
		//If user picks scissors
		case "scissors":
			switch (cpuChoice) {
			//scissors rock
			case "rock":
				losses++;
				return "Computer plays rock and computer wins";
			//scissors paper
			case "paper":
				wins++;
				return "Computer plays paper and computer loses";
			case "scissors":
				ties++;
				return "Computer plays scissors and there was a tie";
			}
			break;
		}
		return null;
	}
	
}